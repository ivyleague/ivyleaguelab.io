import { Injectable, Output, EventEmitter } from '@angular/core';

@Injectable()
export class SideBarService {

  isOpen = false;

  @Output() myChange: EventEmitter<boolean> = new EventEmitter();

  toggle() {
    this.isOpen = !this.isOpen;
    this.myChange.emit(this.isOpen);
  }

}
