import { Component, HostBinding, OnInit } from '@angular/core';
import { SideBarService } from './side-bar.service';

@Component({
  selector: './app-side-bar',
  templateUrl: './side-bar.component.html',
  styleUrls: ['./side-bar.component.sass']
})
export class SideBarComponent implements OnInit {

  @HostBinding('class.is-open')
  isOpen = false;

  constructor(
    private sideBarService: SideBarService
  ) { }

  ngOnInit() {
    this.sideBarService.myChange.subscribe((isOpen: boolean) => {
      this.isOpen = isOpen;
      console.log({sidebar: this});
    });
  }

}
